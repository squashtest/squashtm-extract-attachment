/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.extract;

public class Attachment {

    private Long attachmentListId;
    private Long attachmentContentId;
    private byte[] content;
    private String path;

    public Long getAttachmentListId() {
        return attachmentListId;
    }

    public void setAttachmentListId(Long attachmentListId) {
        this.attachmentListId = attachmentListId;
    }

    public Long getAttachmentContentId() {
        return attachmentContentId;
    }

    public void setAttachmentContentId(Long attachmentContentId) {
        this.attachmentContentId = attachmentContentId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
