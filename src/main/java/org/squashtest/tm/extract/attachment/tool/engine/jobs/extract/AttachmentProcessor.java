/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.extract;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AttachmentProcessor implements ItemProcessor<Attachment,Attachment> {

    private static final String SEPARATOR = File.separator;

    @Value("#{jobParameters[fileRepo]}")
    private String fileRepo;

    @Override
    public Attachment process(Attachment attachment) throws Exception {
        attachment.setPath(getAttachmentPath(attachment.getAttachmentContentId(), attachment.getAttachmentListId()));
        return attachment;
    }

    /**
     * Calculate the file path for an attachment.
     * The path is defined as : the attachment list id / attachment CONTENT id
     * The attachment list id is padded to 12 chars to have a for level hierarchy
     * ex : For an attachment list id of 9856 and a CONTENT ID of 56897 path will be
     * /000/000/009/856/56897
     * @param contentId the ATTACHMENT_CONTENT_ID
     * @return the path as specified above
     */
    private String getAttachmentPath(Long contentId, Long attachmentListId) {
        String folderPath = findFolderPath(attachmentListId);
        return folderPath + contentId;
    }

    /**
     * Calculate the folder path for an attachment list.
     * The path is defined as : the attachment list id.
     * The attachment list id is padded to 12 chars to have a for level hierarchy
     * ex : For an attachment list id of 9856 folder path will be
     * /000/000/009/856
     * @param attachmentListId the attachment list ID
     * @return the path as specified above
     */
    private String findFolderPath(long attachmentListId) {
        String id = String.valueOf(attachmentListId);
        String paddedId = StringUtils.leftPad(id, 12, "0");
        List<String> parts = new ArrayList<>();
        parts.add(paddedId.substring(0, 3));
        parts.add(paddedId.substring(3, 6));
        parts.add(paddedId.substring(6, 9));
        parts.add(paddedId.substring(9, 12));
        String path = StringUtils.join(parts, SEPARATOR);
        String checkedFileRepo = StringUtils.appendIfMissing(fileRepo, SEPARATOR);
        path = checkedFileRepo + path;
        return StringUtils.appendIfMissing(path, SEPARATOR);
    }
}
