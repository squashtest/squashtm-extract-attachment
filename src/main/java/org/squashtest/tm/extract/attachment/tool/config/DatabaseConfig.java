/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties(DatabaseProperties.class)
public class DatabaseConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfig.class);

    @Autowired
    private SpringBatchSchemaHolder schemaHolder;

    @Bean
    @Primary
    public DataSource dataSource(DatabaseProperties databaseProperties) {
        return DataSourceBuilder
                .create()
                .url(databaseProperties.getUrl())
                .username(databaseProperties.getUsername())
                .password(databaseProperties.getPassword())
                .type(org.apache.tomcat.jdbc.pool.DataSource.class)
                .build();
    }

    //second datasource, only because postgresql is not able to manipulate BLOBS with autocommit on
    //AND spring batch is unable to run properly with auutocommit off :-(
    // So two datasource to make these two guys happy...
    @Bean("nonAutoCommitDataSource")
    public DataSource nonAutoCommitDataSource(DatabaseProperties databaseProperties) {
        DataSource dataSource = DataSourceBuilder
                .create()
                .url(databaseProperties.getUrl())
                .username(databaseProperties.getUsername())
                .password(databaseProperties.getPassword())
                .type(org.apache.tomcat.jdbc.pool.DataSource.class)
                .build();

        // Manually disabling autocommit. The properties magic doesn't worked, so let's go doing that ourselves
        // We need to disable autocommit because Postgresql refuse to handle blob when autocommit is on, even in the case of a jdbc reader witch is transactional... weird...
        org.apache.tomcat.jdbc.pool.DataSource dataSourceImpl = (org.apache.tomcat.jdbc.pool.DataSource) dataSource;
        dataSourceImpl.setDefaultAutoCommit(false);
        return dataSource;
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        LOGGER.info("INIT DB");
        final DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setDatabasePopulator(databasePopulator());
        return initializer;
    }

    private DatabasePopulator databasePopulator() {
        final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScript(schemaHolder.getCleanSchema());
        populator.addScript(schemaHolder.getPopulateSchema());
        return populator;
    }

}
