#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) Henix, henix.fr
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software. If not, see <http://www.gnu.org/licenses/>.
#


# DataBase parameters, should be the same that your squash tm db properties
DB_URL=jdbc:h2:../data/squash-tm           # DataBase URL
DB_USERNAME=sa                             # DataBase username
DB_PASSWORD=sa                             # DataBase password
DB_TYPE=h2                                 # Database type, one of h2, mysql, postgresql

# Path to repository
REPO_PATH=./attachments

# Extra Java args
JAVA_ARGS="-Xms128m -Xmx2048m "

# Default variables
JAR_NAME="./squash-tm-extract-attachment-tool.jar"  # Java program

export _JAVA_OPTIONS="-Dspring.datasource.url=${DB_URL} -Dspring.datasource.username=${DB_USERNAME} -Dspring.datasource.password=${DB_PASSWORD} -Duser.language=en -Dsquash.path.file.repository=${REPO_PATH}"

DAEMON_ARGS="${JAVA_ARGS} -jar ${JAR_NAME} --spring.profiles.active=${DB_TYPE} "

echo "$0 : starting Squash TM Attachment extraction tool... ";

exec java ${DAEMON_ARGS}