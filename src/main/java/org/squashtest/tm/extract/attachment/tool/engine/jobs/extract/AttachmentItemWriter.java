/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.extract;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.List;

public class AttachmentItemWriter implements ItemWriter<Attachment> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentItemWriter.class);

    @Override
    public void write(List<? extends Attachment> attachments) throws Exception {
        for (Attachment attachment : attachments) {
            if (attachment.getContent() != null) {
                LOGGER.info("Attachment content : {} Size : {} Path will be {}", attachment.getAttachmentContentId(), attachment.getContent().length, attachment.getPath());
                File file = new File(attachment.getPath());
                FileUtils.copyInputStreamToFile(new ByteArrayInputStream(attachment.getContent()), file);
            } else {
                LOGGER.warn("Attachment content : {} has null stream content", attachment.getAttachmentContentId());
            }
        }
    }
}
