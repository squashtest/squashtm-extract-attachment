@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) Henix, henix.fr
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software. If not, see <http://www.gnu.org/licenses/>.
@REM

REM DataBase parameters, should be the same that your squash tm db properties
REM DB_TYPE can be one of h2, mysql, postgresql
set DB_TYPE=h2
set DB_URL=jdbc:h2:./data/squash-tm
set DB_USERNAME=sa
set DB_PASSWORD=sa

REM Path to the folder where attachments will be extracted
set REPO_PATH=./attachments

REM Technical config
set JAR_NAME=./squash-tm-extract-attachment-tool.jar
set JAVA_ARGS=-Xms128m -Xmx2048m

REM Java arguments
set _JAVA_OPTIONS=-Dspring.datasource.url=%DB_URL% -Dspring.datasource.username=%DB_USERNAME% -Dspring.datasource.password=%DB_PASSWORD% -Duser.language=en -Dsquash.path.file.repository=%REPO_PATH%

set DAEMON_ARGS=%JAVA_ARGS% -jar %JAR_NAME% --spring.profiles.active=%DB_TYPE%

echo Starting Squash TM Attachment extraction tool
java %DAEMON_ARGS%
pause