/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.verify;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.squashtest.tm.extract.attachment.tool.engine.jobs.extract.Attachment;
import org.squashtest.tm.extract.attachment.tool.engine.jobs.extract.AttachmentProcessor;
import org.squashtest.tm.extract.attachment.tool.engine.jobs.extract.ExtractJobConfig;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Configuration
@Import(ExtractJobConfig.class)
public class VerifyJobConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JobBuilderFactory jobFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    @Qualifier("attachmentItemReader")
    private ItemReader<Attachment> attachmentItemReader;

    @Autowired
    @Qualifier("attachmentItemProcessor")
    private ItemProcessor<Attachment, Attachment> attachmentItemProcessor;

    @Bean
    public Job verifyJob(){
        return jobFactory.get("verifyJob")
                .start(verifyStep())
                .build();
    }

    @Bean
    public Step verifyStep() {
        return stepBuilderFactory.get("VerifyStep")
                .<Attachment, Attachment>chunk(500)
                .reader(attachmentItemReader)
                .processor(compositeItemProcessor(verifyProcessor()))
                .writer(noopWriter())
                .build();
    }

    @Bean
    public ItemProcessor<Attachment, Attachment> verifyProcessor() {
        return new VerifyProcessor();
    }

    @Bean
    public ItemWriter<Attachment> noopWriter() {
        return new NoOpItemWriter();
    }

    @Bean
    @StepScope
    public CompositeItemProcessor<Attachment,Attachment> compositeItemProcessor (ItemProcessor<Attachment,Attachment> verifyProcessor){
        CompositeItemProcessor composite = new CompositeItemProcessor();
        List<ItemProcessor<Attachment,Attachment>> itemProcessors = new ArrayList<>();
        itemProcessors.add(attachmentItemProcessor);
        itemProcessors.add(verifyProcessor);
        composite.setDelegates(itemProcessors);
        return composite;
    }

}
