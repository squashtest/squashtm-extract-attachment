/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
@Profile("mysql")
public class MysqlSchemaHolder implements SpringBatchSchemaHolder {

    @Value("classpath:/org/springframework/batch/core/schema-drop-mysql.sql")
    private Resource cleanSchema;

    @Value("classpath:/org/springframework/batch/core/schema-mysql.sql")
    private Resource createSchema;

    @Override
    public Resource getCleanSchema() {
        return cleanSchema;
    }

    @Override
    public Resource getPopulateSchema() {
        return createSchema;
    }
}
