/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.verify;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.squashtest.tm.extract.attachment.tool.engine.jobs.extract.Attachment;

import java.io.File;
import java.security.MessageDigest;

public class VerifyProcessor implements ItemProcessor<Attachment, Attachment> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerifyProcessor.class);

    @Override
    public Attachment process(Attachment attachment) throws Exception {
        if (attachment.getContent() != null) {
            String path = attachment.getPath();
            byte[] fileContent = FileUtils.readFileToByteArray(new File(path));
            byte[] databaseContent = attachment.getContent();
            boolean equal = MessageDigest.isEqual(fileContent, databaseContent);
            if (equal) {
                LOGGER.info("Attachment {} and file {} are equals.", attachment.getAttachmentContentId(), attachment.getPath());

            } else {
                String msg = String.format("Copy error for attachment content %d in attachment list %d. The database content is not identical to the file content for file %s. Check your parameters, disk size, permissions... and retry the whole migration process",
                        attachment.getAttachmentContentId(), attachment.getAttachmentListId(), attachment.getPath());
                throw new RuntimeException(msg);
            }
        }
        return attachment;
    }
}
