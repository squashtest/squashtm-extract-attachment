/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.extract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AttachmentRowMapper implements RowMapper<Attachment> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentRowMapper.class);

    @Override
    public Attachment mapRow(ResultSet resultSet, int i) throws SQLException {
        Attachment attachment = new Attachment();
        attachment.setAttachmentListId(resultSet.getLong("ATTACHMENT_LIST_ID"));
        attachment.setAttachmentContentId(resultSet.getLong("ATTACHMENT_CONTENT_ID"));
        byte[] content = getContent(resultSet);
        attachment.setContent(content);

        return attachment;
    }

    private byte[] getContent(ResultSet resultSet) throws SQLException, RuntimeException {
        Blob streamContent = resultSet.getBlob("STREAM_CONTENT");
        // Issue 7669 - stream content can be null
        if (streamContent != null) {
            //in jdbc, blob byte count begin to 1 and they subtract 1 in getByte methods... maybe a strange SQL specification...
            return streamContent.getBytes(1, (int) streamContent.length());
        } else {
            return null;
        }
    }
}
