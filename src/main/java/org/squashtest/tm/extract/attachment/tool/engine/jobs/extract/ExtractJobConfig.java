/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine.jobs.extract;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.AbstractSqlPagingQueryProvider;
import org.springframework.batch.item.database.support.H2PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.batch.item.database.support.PostgresPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class ExtractJobConfig {

    private static final String SELECT_CLAUSE = "SELECT ATTACHMENT_CONTENT_ID, ATTACHMENT_LIST_ID, STREAM_CONTENT, ATTACHMENT_ID ";
    private static final String FROM_CLAUSE = "FROM ATTACHMENT\n" +
            "  INNER JOIN ATTACHMENT_CONTENT ON ATTACHMENT.CONTENT_ID = ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID ";

    @Autowired
    @Qualifier("nonAutoCommitDataSource")
    private DataSource nonAutoCommitDataSource;

    @Autowired
    public JobBuilderFactory jobFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private Environment environment;

    @Bean
    public Job extractAttachment() {
        return jobFactory.get("extractJob")
                .start(extractAttachmentStep())
                .build();
    }

    @Bean
    public Step extractAttachmentStep() {
        return stepBuilderFactory.get("extractAttachmentStep")
                .allowStartIfComplete(true)
                .<Attachment, Attachment>chunk(10)
                .reader(attachmentItemReader())
                .processor(attachmentItemProcessor())
                .writer(attachmentItemWriter())
                .build();
    }


    @Bean
    @StepScope
    public JdbcPagingItemReader<Attachment> attachmentItemReader() {
        JdbcPagingItemReader<Attachment> itemReader = new JdbcPagingItemReader<>();
        itemReader.setDataSource(nonAutoCommitDataSource);
        itemReader.setQueryProvider(mySqlPagingQueryProvider());
        itemReader.setRowMapper(new AttachmentRowMapper());
        return itemReader;
    }

    @Bean
    public PagingQueryProvider mySqlPagingQueryProvider() {
        AbstractSqlPagingQueryProvider queryProvider;
        List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());
        if (activeProfiles.contains("mysql")) {
            queryProvider = new MySqlPagingQueryProvider();
        } else if (activeProfiles.contains("postgresql")){
            queryProvider = new PostgresPagingQueryProvider();
        } else if(activeProfiles.contains("h2")){
            queryProvider = new H2PagingQueryProvider();
        } else {
            throw new IllegalArgumentException("Unknown profile " + activeProfiles);
        }
        queryProvider.setSelectClause(SELECT_CLAUSE);
        queryProvider.setFromClause(FROM_CLAUSE);
        queryProvider.setSortKeys(sortAttachmentId());
        return queryProvider;
    }

    private Map<String, Order> sortAttachmentId() {
        Map<String, Order> sortConfiguration = new HashMap<>();
        sortConfiguration.put("ATTACHMENT_ID", Order.ASCENDING);
        return sortConfiguration;
    }

    @Bean
    public AttachmentItemWriter attachmentItemWriter() {
        return new AttachmentItemWriter();
    }

    @Bean
    @StepScope
    ItemProcessor<Attachment, Attachment> attachmentItemProcessor() {
        return new AttachmentProcessor();
    }

}
