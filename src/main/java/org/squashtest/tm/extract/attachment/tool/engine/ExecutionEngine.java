/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software. If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.extract.attachment.tool.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class ExecutionEngine {

    @Value("${squash.path.file.repository}")
    private String fileRepo;

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionEngine.class);

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    @Qualifier("extractAttachment")
    private Job extractAttachmentJob;

    @Autowired
    @Qualifier("verifyJob")
    private Job verifyJob;

    @Autowired
    @Qualifier("deletionJob")
    private Job deletionJob;

    @Autowired
    private DataSource dataSource;

    public void runMainJobMigration() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobExecution extractJobExecution = executeExtractJob();
        if (extractJobExecution.getStatus().equals(BatchStatus.COMPLETED)) {
            LOGGER.info("################ Attachments copied to system file. Proceeding to verifications before deletion in database. ################");
            JobExecution verifyJobExecution = executeVerifyJob();
            if (verifyJobExecution.getStatus().equals(BatchStatus.COMPLETED)) {
                LOGGER.info("################ Attachments checked. Proceeding to deletion in database. ################");
                JobExecution deletionJobExecution = executeDeletionJob();
                if (deletionJobExecution.getStatus().equals(BatchStatus.COMPLETED)) {
                    LOGGER.info("################ Migration complete ################");
                } else {
                    LOGGER.error("################ Error while deleting attachment from database ################");
                }
            }
        }
    }

    private JobExecution executeExtractJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        JobParameters jobParameters = getJobParameters();
        return jobLauncher.run(extractAttachmentJob, jobParameters);
    }

    private JobExecution executeVerifyJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        JobParameters jobParameters = getJobParameters();
        return jobLauncher.run(verifyJob, jobParameters);
    }

    private JobExecution executeDeletionJob() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
        JobParameters jobParameters = getJobParameters();
        return jobLauncher.run(deletionJob, jobParameters);
    }

    private JobParameters getJobParameters() {
        Map<String, JobParameter> params = new HashMap();
        params.put("fileRepo", new JobParameter(fileRepo));
        params.put("executionDate", new JobParameter(new Date()));
        return new JobParameters(params);
    }

}
